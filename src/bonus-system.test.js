import { calculateBonuses } from "./bonus-system.js";
const assert = require("assert");

describe('Bonus-system tests', () => {
    let app;
    console.log("Tests started");

    test('Invalid program test', (done) => {
        let bonus = calculateBonuses('Test', 1000);
        assert.equal(bonus, 0);
        done();
    });

    let standardMultiplier = 0.05;
    let premiumMultiplier = 0.1;
    let diamondMultiplier = 0.2;

    let firstBonusMultiplier = 1;
    let secondBonusMultiplier = 1.5;
    let thirdBonusMultiplier = 2;
    let fourthBonusMultiplier = 2.5;

    test('Standard program test', (done) => {
        let bonus = calculateBonuses('Standard', 1000);
        assert.equal(bonus, standardMultiplier * firstBonusMultiplier);
        done();
    });

    test('Premium program test', (done) => {
        let bonus = calculateBonuses('Premium', 1000);
        assert.equal(bonus, premiumMultiplier * firstBonusMultiplier);
        done();
    });

    test('Diamond program test', (done) => {
        let bonus = calculateBonuses('Diamond', 1000);
        assert.equal(bonus, diamondMultiplier * firstBonusMultiplier);
        done();
    });

    test('First bonus boundary test', (done) => {
        let bonus = calculateBonuses('Standard', 10000);
        assert.notEqual(bonus, standardMultiplier * firstBonusMultiplier);
        done();
    });

    test('Second bonus test', (done) => {
        let bonus = calculateBonuses('Standard', 20000);
        assert.equal(bonus, standardMultiplier * secondBonusMultiplier);
        done();
    });

    test('Second bonus boundary test', (done) => {
        let bonus = calculateBonuses('Standard', 50000);
        assert.notEqual(bonus, standardMultiplier * secondBonusMultiplier);
        done();
    });

    test('Third bonus test', (done) => {
        let bonus = calculateBonuses('Standard', 60000);
        assert.equal(bonus, standardMultiplier * thirdBonusMultiplier);
        done();
    });

    test('Third bonus boundary test', (done) => {
        let bonus = calculateBonuses('Standard', 100000);
        assert.notEqual(bonus, standardMultiplier * thirdBonusMultiplier);
        done();
    });

    test('Fourth bonus test', (done) => {
        let bonus = calculateBonuses('Standard', 110000);
        assert.equal(bonus, standardMultiplier * fourthBonusMultiplier);
        done();
    });

    console.log('Tests Finished');
});
